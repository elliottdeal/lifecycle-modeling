ACS/census lifecycle modeling
=============================

How does consumption change over a person's life? The idea is to see whether, for example, a 20-year-old today lives similarly to a 20-year old 5 or 10 years earlier. Does a 35-year-old's life look the same as that person's parents' life at the same age?

But this is a work in progress. But I'll start with simple profiles of different ages.


Warnings!
=============
These scripts use the "basic" codebook (.cbk) file and the Stata (.do) command file. Be sure to download both corresponding to your extract.

- The codebook file is used to parse the fixed-width raw file (get column widths), and to build the lookup tables to add labels to coded variables.
- The Stata command file is used to find any float variables represented as integers (PERWT, HHWT, etc.).

This project's main data sets and derived (script-generated) results aren't tracked with git; they are large. The codebook or command files can be used to download the data from IPUMS (once you create an account, etc.).

The shell pre-processing script was written to work on OS X (10.13.3). It uses commands like gunzip and grep. I doubt these will work on a Windows machine. I'm not sure about other OSes.


Source
==========
Steven Ruggles, Katie Genadek, Ronald Goeken, Josiah Grover, and Matthew Sobek. Integrated Public Use Microdata Series: Version 7.0 [dataset]. Minneapolis: University of Minnesota, 2017. https://doi.org/10.18128/D010.V7.0


Workflow
============
The first few scripts process the raw data. That's as far as I've gotten.


Data
========
usa_00001
- First download, mainly used to get the basic data structure while writing lookup table scripts, etc.

usa_00002
- Added 1990 and 2000 samples plus many more variables for actual analysis.

usa_00003
- Removed 1990 sample to save about 3GB of space for easier preliminary and because a fair number of variables were not available for that year.


Scripts
===========
globals.R
- Sets some project-wide constants, such as the file being used (e.g., usa_0001).

10-shell preprocessing.R
- Splits raw data into household and person files.

20-read-fixed-width-files.R
- Used to find column widths for reading fixed-width person and household files, derived from the raw, hierarchical download, and actually reads them.

30-build-lookup-table.R
- Creates lookup tables for each variable, to add human-readable labels.

40-post-read-processing.R
- Some basic processing, adding column labels, checking for topcoding, ...

50-explore.R
- Initial exploration of variables.

execute-analysis.R
- Will be used to run entire analysis when project's complete.
- During prototyping: just runs scripts 10-40.


Todo
========
Add system/session specs when done.

